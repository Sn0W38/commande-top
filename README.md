[![ Github All Releases ](https://img.shields.io/badge/RELEASE-V%201.1-blue?style=for-the-badge)](https://gitlab.com/Sn0W38/commande-top)
#  Gestionnaire de commande à top
Outils permettant de générer des requêtes SQL pour le logiciel PMI

###  Utilisation de la ligne de commande
Génère un build ``` .Exe ```, pour faciliter la prise en main utilisateur.

####  Build du programme sous Windows :
```shell
  pyinstaller --onefile -n=command-top --icon=ico.ico .\main.py
```
#### Utilisation

1. Le programme utilise en chemin root son emplacement actuel
   1. Le logiciel prend en charge uniquement:
      1. ``` .xlsx ```
      2. ``` .xls ```
         1. Ce type de format est pris en charge dans la version 1.1 (le programme converti le format .xls -> .xlsx)
2. Sélection du fichier
   1. Liste de choix contenu dans le dossier source
3. Vérification des intitulés de colonne
4. Parcours et création temporaire du résultat
   1. Possibilité d'annuler la génération du document
5. Demande d'extraction des données
6. Création d'un fichier ```.txt``` contenant les requêtes SQL

## Installations
* pip install py-console
* pip install inquirer
* pip install pywin32
* pip install -U pyinstaller

### Historique de mise à jour

<details>
<summary>V1.1</summary>

* Ne permet plus à l'utilisateur de selectionner un chemin 'root' different
  * Forcer à la racine de l'exe: ./
* Convertie les fichiers XLS en XLSX
</details>


<details>
<summary>V1.0</summary>
* Selection du chemin root
* Vérification des intitulé de colonne
* Extraction des requêtes SQL
</details>

## Authors

[@kevinlandry](https://gitlab.com/Sn0W38)
