# Import python
import os
import pprint

import inquirer
import openpyxl
import glob

from pathlib import Path
from datetime import date
from typing import TextIO

import win32com.client

from py_console import console

result_lcomfou = []
result_ecomfou = []
result_lrecfou = []
result_erecfou = []

result_lcomfou_fact = []
result_ecomfou_fact = []
result_lrecfou_fact = []
result_erecfou_fact = []

result = {}
result_fact = {}

# ==============================================================================
# préparation header du fichier
headers = {
    'LCOMFOU': '#######################################' + '\r' +
               '############### LCOMFOU ###############' + '\r' +
               '#######################################' + '\r',
    'ECOMFOU': '#######################################' + '\r' +
               '############### ECOMFOU ###############' + '\r' +
               '#######################################' + '\r',
    'LRECFOU': '#######################################' + '\r' +
               '############### LRECFOU ###############' + '\r' +
               '#######################################' + '\r',
    'ERECFOU': '#######################################' + '\r' +
               '############### ERECFOU ###############' + '\r' +
               '#######################################' + '\r'
}

# Préparation des selects
select = {
    'LCOMFOU': "SELECT * FROM LCOMFOU" + "\r" +
               "WHERE" + "\r",
    'ECOMFOU': "SELECT * FROM ECOMFOU" + "\r" +
               "WHERE" + "\r",
    'LRECFOU': "SELECT * FROM LRECFOU" + "\r" +
               "WHERE" + "\r",
    'ERECFOU': "SELECT * FROM ERECFOU" + "\r" +
               "WHERE" + "\r"
}

select_light = {
    'ECOMFOU': "SELECT * FROM ECOMFOU" + "\r" +
               "WHERE" + "\r",
    'ERECFOU': "SELECT * FROM ERECFOU" + "\r" +
               "WHERE" + "\r"
}


# ==============================================================================


def import_files(routes: str, filename: str) -> dict:
    console.success('Recherche en cours...')
    value = {}
    result_req = {}

    xlsx_file = Path(routes, filename)

    wb_obj = openpyxl.load_workbook(xlsx_file)

    sheet_max_row = wb_obj.worksheets[0].max_row

    console.success('Nombre de ligne détecté dans le fichier : ' + str(sheet_max_row))

    sheet_active = wb_obj.active
    # Nombre maximal de ligne
    max_row = 0

    stop = True

    while stop:
        max_row += 1
        if sheet_active['A' + str(max_row)].value and sheet_active['A' + str(max_row)].value is not None:
            tmp = {}
            # Vérifie les intitulés de colonnes
            # Bloque le chargement des lignes en cas d'erreur
            isOk = True

            if max_row == 1:
                console.info('Vérification des intitulés de colonnes en cours...')

            # Cellule A == N°Bon
            if sheet_active['A1'].value == 'N°Bon':
                tmp['A' + str(max_row)] = sheet_active['A' + str(max_row)].value
            else:
                isOk = False
                console.error('Cellule: N°Bon -> introuvable !')
                console.info('A' + str(max_row))

            # Cellule C == Ligne
            if sheet_active['C1'].value == 'Ligne':
                tmp['C' + str(max_row)] = sheet_active['C' + str(max_row)].value
            else:
                isOk = False
                console.error('Cellule: Ligne -> introuvable !')
                console.info('C' + str(max_row))

            # Cellule D == Indice Rec
            if sheet_active['D1'].value == 'Indice Rec':
                tmp['D' + str(max_row)] = sheet_active['D' + str(max_row)].value
            else:
                isOk = False
                console.error('Cellule: Indice Rec -> introuvable !')
                console.info('D' + str(max_row))

            # Cellule T == N°Facture
            if sheet_active['T1'].value == 'N°Facture':
                tmp['T' + str(max_row)] = sheet_active['T' + str(max_row)].value
            else:
                isOk = False
                console.error('Cellule: N°Facture -> introuvable !')
                console.warn('Une erreur est trouver ligne n°' + str(max_row))
                console.warn('Cellule problèmatique -> T' + str(max_row))

            if isOk:
                # Ne prend pas en compte la 1re cellule (cellule titre)
                if len(tmp) != 0:

                    if tmp['A' + str(max_row)] is None and tmp['C' + str(max_row)] is None and tmp[
                        'D' + str(max_row)] is None:
                        return value
                    else:
                        if max_row > 1:
                            # Index les résultats dans le dictionnaire
                            value['A' + str(max_row) + '-' + 'C' + str(max_row) + '-' + 'D' + str(max_row)] = {
                                'A': tmp['A' + str(max_row)],
                                'C': tmp['C' + str(max_row)],
                                'D': tmp['D' + str(max_row)],
                                'T': tmp['T' + str(max_row)],
                                'Cell': max_row
                            }
                    console.success('Traitement de la ligne -> ' + str(max_row) + '/' + str(sheet_max_row))

            else:
                console.error('Erreur lors de la recherche des champs disponible : les intitulés de colonnes '
                              'comprennent des erreurs.')
                stop = False
        else:
            max_row += -1
            stop = False

    return value


def new_prepare_req(value: dict):
    # Tableau des lignes avec numéro factures
    global result_fact, result, result_none
    arr_num_fact = []
    # Tableau sans numéro de facture
    arr_none = []
    # Champ OR de la requête SQL
    add_or = 'OR'
    # Date du jour
    today = date.today()
    # YY-mm-dd
    d2 = today.strftime("%Y%m%d")

    for i in value:
        # Valeur de la cellule A + int
        a = ''
        # Valeur de la cellule A + int
        c = ''
        # Valeur de la cellule D + int
        d = ''
        # Valeur de la cellule T + int
        f = ''

        value_lcom = ''
        value_ecom = ''
        value_lrec = ''
        value_erec = ''

        result_none = []
        result_fact = []
        result = {}

        # N°Bon
        if value[i].get('A'):
            a = value[i]['A']

        # Ligne
        if value[i].get('C'):
            c = value[i]['C']

        # Indice Rec
        if value[i].get('D'):
            d = value[i]['D']

        # Numéro de facture
        if value[i].get('T'):
            f = value[i]['T']

        if f and str(f).upper() != 'REGUL':
            arr_num_fact.append({'A': a, 'C': c, 'D': d, 'F': f})
            arr_none.append({'A': a, 'C': c, 'D': d, 'F': f})

        else:
            arr_none.append({'A': a, 'C': c, 'D': d, 'F': 'REGUL'})

    if len(arr_num_fact) != 0:
        value_lcom = ''
        value_lrec = ''

        # Boucle pour chaque ligne
        for x in arr_num_fact:
            A = x['A']
            C = x['C']
            D = x['D']
            F = x['F']

            value_ecom = f"(ECKTSOC='100' AND ECKTNUMERO='{A}')"
            value_erec = f"(ECKTSOC='100' AND ECKTNUMERO='{A}' AND ECKTINDICE='{D}')"

            # Préparation des updates
            update = {
                'ECOMFOU': "UPDATE ECOMFOU" + "\r" +
                           f"SET ECCTDERFA='{F}'" + "\r" +
                           "WHERE" + "\r",
                'ERECFOU': "UPDATE ERECFOU" + "\r" +
                           "SET ECCTTOP02='O'," + "\r" +
                           f"ECCJDERFA='{d2}'," + "\r" +
                           f"ECCTDERFA='{F}'" + "\r" +
                           "WHERE" + "\r"
            }

            result_fact.append({'num': F, 'data': {
                'ECOMFOU': value_ecom,
                'ERECFOU': value_erec,
                'UPDATE': update,
            }})

    if len(arr_none) != 0:
        value_lcom = ''
        value_ecom = ''
        value_lrec = ''
        value_erec = ''

        # Nombre de ligne dans l'arr
        max_length_none = len(arr_none)

        for x in arr_none:
            A = x['A']
            C = x['C']
            D = x['D']
            F = x['F']

            if len(result_lcomfou) + 1 == max_length_none or len(result_ecomfou) + 1 == max_length_none or len(
                    result_lrecfou) + 1 == max_length_none or len(result_erecfou) + 1 == max_length_none:
                if len(F) != 'REGUL':
                    value_lcom = f"(LCKTSOC='100' AND LCKTNUMERO='{A}' AND LCKTLIGNE='{C}')" + '\r'
                value_ecom = f"(ECKTSOC='100' AND ECKTNUMERO='{A}')" + '\r'
                if len(F) != 'REGUL':
                    value_lrec = f"(LCKTSOC='100' AND LCKTNUMERO='{A}' AND LCKTLIGNE='{C}' AND LCKTPSF='{D}')" + '\r'
                value_erec = f"(ECKTSOC='100' AND ECKTNUMERO='{A}' AND ECKTINDICE='{D}')" + '\r'
            else:
                if len(F) != 'REGUL':
                    value_lcom = f"(LCKTSOC='100' AND LCKTNUMERO='{A}' AND LCKTLIGNE='{C}') {add_or}" + '\r'
                value_ecom = f"(ECKTSOC='100' AND ECKTNUMERO='{A}') {add_or}" + '\r'
                if len(F) != 'REGUL':
                    value_lrec = f"(LCKTSOC='100' AND LCKTNUMERO='{A}' AND LCKTLIGNE='{C}' AND LCKTPSF='{D}') {add_or}" + '\r'
                value_erec = f"(ECKTSOC='100' AND ECKTNUMERO='{A}' AND ECKTINDICE='{D}') {add_or}" + '\r'

            if len(F) != 'REGUL':
                result_lcomfou.append(value_lcom)
                result_lrecfou.append(value_lrec)

            result_ecomfou.append(value_ecom)
            result_erecfou.append(value_erec)

        result_none = {
            'LCOMFOU': result_lcomfou,
            'ECOMFOU': result_ecomfou,
            'LRECFOU': result_lrecfou,
            'ERECFOU': result_erecfou
        }

    print('Nombre de ligne traiter ', len(result_ecomfou))

    return {
        'NONE': result_none,
        'FACT': result_fact
    }


def write_files(value: dict, filename: str):
    today = date.today()
    # YY-mm-dd
    d1 = today.strftime("%Y-%m-%d")
    d2 = today.strftime("%Y%m%d")
    # Crée le fichier
    text_file = open(f"{d1}-{filename}.txt", "w")

    # Préparation des updates
    update = {
        'LCOMFOU': "UPDATE LCOMFOU" + "\r" +
                   "SET LCCTSOLACC='S'," + "\r" +
                   "LCCNVOLCON=LCCNQTECDE," + "\r" +
                   "LCCNQTECON=LCCNQTECDE" + "\r" +
                   "WHERE" + "\r",
        'ECOMFOU': "UPDATE ECOMFOU" + "\r" +
                   "SET ECCTDERFA='REGUL'" + "\r" +
                   "WHERE" + "\r",
        'LRECFOU': "UPDATE LRECFOU" + "\r" +
                   "SET LCCTSOLACC='S'" + "\r" +
                   "WHERE" + "\r",
        'ERECFOU': "UPDATE ERECFOU" + "\r" +
                   "SET ECCTTOP02='O'," + "\r" +
                   f"ECCJDERFA='{d2}'," + "\r" +
                   "ECCTDERFA='REGUL'" + "\r" +
                   "WHERE" + "\r"
    }

    if len(value['NONE']) != 0:
        for i in value['NONE']:

            text_file.write(headers[i])
            text_jump(text_file)
            text_file.write(select[i])

            for k in value['NONE'][i]:
                text_file.write(k)

            text_jump(text_file)

            text_file.write(update[i])

            for k in value['NONE'][i]:
                text_file.write(k)

            text_jump(text_file)

    text_file.write('***************************************' + '\r' +
                    '***************** SWAP ****************' + '\r' +
                    '************** AVEC NUMERO ************' + '\r' +
                    '***************************************' + '\r')
    text_jump(text_file)

    arr_test = {}

    if len(value['FACT']) != 0:

        for i in value['FACT']:

            tmp_ecom = []
            tmp_erec = []

            if i['num'] in arr_test:

                # Supprime le dernier saut de ligne de la dernière valeur du tableau
                arr_test[i['num']]['ECOMFOU'][-1] = arr_test[i['num']]['ECOMFOU'][-1][:-1]
                arr_test[i['num']]['ERECFOU'][-1] = arr_test[i['num']]['ERECFOU'][-1][:-1]

                # Stock la valeur du tableau
                tmp_ecom = arr_test[i['num']]['ECOMFOU']
                tmp_erec = arr_test[i['num']]['ERECFOU']

                # Ajoute un saut de ligne et un 'OR'
                tmp_ecom.append(' OR\r' + i['data']['ECOMFOU'] + '\r')
                tmp_erec.append(' OR\r' + i['data']['ERECFOU'] + '\r')

                arr_test[i['num']] = {
                    'ECOMFOU': tmp_ecom,
                    'ERECFOU': tmp_erec,
                    'UPDATE': i['data']['UPDATE']
                }

            else:
                arr_test[i['num']] = {
                    'ECOMFOU': [i['data']['ECOMFOU'] + '\r'],
                    'ERECFOU': [i['data']['ERECFOU'] + '\r'],
                    'UPDATE': i['data']['UPDATE']
                }

        cons = ['ECOMFOU', 'ERECFOU']

        for i in arr_test:

            arr = arr_test[i]

            for y in cons:
                text_file.write(headers[y])
                text_jump(text_file)

                text_file.write(select[y])
                for p0 in arr[y]:
                    text_file.write(p0)

                text_jump(text_file)

                text_file.write(arr['UPDATE'][y])

                for p1 in arr[y]:
                    text_file.write(p1)

                text_jump(text_file)

    return


# Saut de ligne
def text_jump(text: TextIO):
    return text.write('\r ========================================================= \r\r')


# Définit les questions pour assuré le bon fonctionnement du script
def question_for_search(rootDirectory, file) -> list:
    continu = True

    while continu:

        while rootDirectory is None:
            tmpRootDirectory = input("Chemin complet du fichier, ou entrer 'x': ")
            # tmpRootDirectory = 'C:\\Users\k.landry\Documents\Commandes-top'
            if tmpRootDirectory is not None:
                if tmpRootDirectory == 'x':
                    rootDirectory = 'null'
                    continu = False
                else:
                    pathExits = os.path.exists(tmpRootDirectory)
                    if pathExits:
                        rootDirectory = tmpRootDirectory
                    else:
                        console.error("Le chemins n'éxiste pas")

        if rootDirectory != 'null':

            while file is None:
                tmpFile = file_found(rootDirectory)
                # tmpFile = 'FNP1-k.xlsx'
                # Au cas où inquirer ne fonctionne plus utiliser input()
                # tmpFile = input("Nom du fichier, ou 'x' ")
                if tmpFile is not None:
                    if tmpFile == 'x':
                        file = 'null'
                        continu = False
                    else:
                        fileExist = os.path.exists(rootDirectory + "\\" + tmpFile)
                        if fileExist:
                            if os.path.splitext(tmpFile)[-1].lower() == '.xlsx':
                                file = tmpFile
                            else:
                                file = 'null'
                                continu = False
                                console.log("Le format du fichier est incorrecte: (Format valide: .XLSX)")
                        else:
                            file = 'null'
                            continu = False
                            console.log("Le fichier n'éxiste pas")
        else:
            file = 'null'

        if file and rootDirectory:
            continu = False

    return [rootDirectory, file]


# Transforme un fichier XLS en XLSX
def check_extension(filename: any) -> str:
    output: str = None
    correct_extension = ['.xls', '.xlsx']
    extension = os.path.splitext(filename)[-1].lower()
    name = os.path.basename(filename)
    file = os.path.abspath(filename)

    if extension in correct_extension:

        if extension == '.xls':
            console.log(f'Pour fonctionner correctement le fichier: ${name}.xls doit être convertie en fichier .XLSX')

            # Initialise EXEL
            App = win32com.client.Dispatch("Excel.Application")
            App.Visible = False

            output_file = name.replace('.xls', '.xlsx')
            workbook = App.Workbooks.Open(file)
            workbook.ActiveSheet.SaveAs(file.replace(name, '') + output_file, 51)  # 51 is for xlsx
            workbook.Close(SaveChanges=True)
            App.Quit()
            # Détruit EXEL
            console.log(f'Fin de la conversion du fichier ${name}.xls')
        elif extension == '.xlsx':
            output = name
        else:
            output = None

    return output


def file_found(rootDirectory: str) -> str:
    result = ''
    file_found = []
    files = glob.glob(rootDirectory + '/*')

    if len(files) != 0:
        for f in files:
            if os.path.isfile(f):
                # Vérifier la présence de la bonne extension sinon convertie le fichier
                filename = check_extension(f)

                if filename is not None:
                    filename = f.replace(rootDirectory, '')
                    if filename[0] == "\\":
                        filename = filename.replace("\\", "")
                    file_found.append(filename)

        if len(file_found) == 0:
            console.error('Aucune fichier valide ne correspond')
        else:
            questions = [
                inquirer.List('filename',
                              message="Quel est le fichier de travail ?",
                              choices=file_found
                              )
            ]

            answers = inquirer.prompt(questions)
            result = answers.get('filename')

    return result
