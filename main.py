from utils import *

console.log('Démarrage du programme de génération SQL...')
# Chemin root du pc
# Chemin ou chercher les fichiers
# rootDirectory = "C:\\Users\k.landry\Documents\Commandes-top\Commandes\\15-03-2022"
# file = 'template-top.xlsx'
# Force l'utilisateur à parcourir les fichiers au même endroit que le prog
rootDirectory = '.\\'
file = None
continu = True
result_req = {}
response = question_for_search(rootDirectory, file)

if len(response) == 2:
    rootDirectory = response[0]
    file = response[1]
    if file != 'null' and rootDirectory != 'null':
        # Valeur des cellules intéressante pour effectuer les requêtes SQL
        value_req = import_files(rootDirectory, file)

        # prépare les requêtes SQL en fonction des données renseigner en amont
        result_req = new_prepare_req(value_req)
        # for i in value_req:
        #     result_req = prepare_req(value_req[i], len(value_req))

        continu = True

        if len(result_req) != 0:
            while continu:
                restart = input('Export des requêtes SQL en fichier .txt : Y ou N ')
                if restart.lower() == 'y':
                    write_files(result_req, file)
                    continu = False
                else:
                    console.warn('Export annuler !!')
                    continu = False

        console.success('Export Terminer')
    else:
        console.log('Recherche annuler')

else:
    response = question_for_search(rootDirectory, file)


